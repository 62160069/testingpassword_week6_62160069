const { checkAlphabet, checkLength, checkPassword, checkDigit, checkSymbol } = require('./password')
describe('Test password length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 characters to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })

  test('should 25 characters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet m in password to be true', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
  test('should has alphabet A in password to be true', () => {
    expect(checkAlphabet('A')).toBe(true)
  })
  test('should has not alphabet in password to be false', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has symbol ! in password to be true', () => {
    expect(checkSymbol('!1111')).toBe(true)
  })

  test('should has not symbol ! in password to be false', () => {
    expect(checkSymbol('1111')).toBe(false)
  })
})
// finish
describe('Test Digit', () => {
  test('should has digit 0 in password to be true', () => {
    expect(checkDigit(0)).toBe(true)
  })
})

describe('Test Password', () => {
  test('should password Boy@12 to be false', () => {
    expect(checkPassword('Bm@123')).toBe(false)
  })
})
